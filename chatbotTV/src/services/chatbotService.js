import request from "request";
import dotenv from 'dotenv';
import messengerService from '../services/messengerService.js';
import date from '../helpers/date.js';
import db from "../database/connection.js";

dotenv.config();
const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;
const OPENAI_API_KEY = process.env.OPENAI_API_KEY;
// hàm lấy thông tin người dùng
const getUserName = (sender_psid) => {
  return new Promise((resolve, reject) => {
    request({
      "uri": `https://graph.facebook.com/${sender_psid}?fields=first_name,last_name,profile_pic&access_token=${PAGE_ACCESS_TOKEN}>`,
      "method": "GET",
    }, (err, res, body) => {
      console.log("---------------------------------------");
      // console.log(`>>>36<<< ${body}`)
      if (!err) {
        body = JSON.parse(body);
        let username = `${body.last_name} ${body.first_name}`;
        resolve(username);
      } else {
        console.error("Unable to send message:" + err);
        reject(err);
      }
    });
  })
}

//hàm xử lý khi bắt đầu
const handleGetstarted = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let username = await getUserName(sender_psid);
      let response1 = { "text": `Xin chào ${username} đến với lịch vạn sự của chúng tôi !` }

      // // send an image
      // let response2 = getImageGetStartedTemplate();

      let response3 = getStartedTemplate();

      //send text message
      await messengerService.callSendAPI(sender_psid, response1);

      //send generic template message
      await messengerService.callSendAPI(sender_psid, response3);

      resolve('done');
    } catch (err) {
      reject(err);
    }
  })
}

//hàm xử lý Tin nhắn hôm nay Lịch
const handleMessageTodayCalendar = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let TodayCalendar = date.getTodayCalendar();
      let response = { "text": `- Hôm nay: ${TodayCalendar.solar}\n- ${TodayCalendar.lunar}` }

      await messengerService.callSendAPI(sender_psid, response);
      // console.log("check response from today calendar: ", response)
      resolve('done');
    } catch (err) {
      reject(err);
    }
  });
}

//hàm xử lý tạo thông báo
const handleCREATE_NOTIFICATIONS = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {

      let response = getLunarTemplate(sender_psid);

      //send text message
      await messengerService.callSendAPI(sender_psid, response);

      resolve('done');
    } catch (err) {
      reject(err);
    }
  })
}

//Hàm xử lý xem lịch đã đặt
const handleViewScheduledEvents = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {

      let response = getAllLunarTemplate(sender_psid);

      //send text message
      await messengerService.callSendAPI(sender_psid, response);

      resolve('done');
    } catch (err) {
      reject(err);
    }
  })
}
// hà xử lý Đặt lịch hàng tháng
const handleSetMonthlyCalendar = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response = getQuickReplyMonthlyCalendar();
      await messengerService.callSendAPI(sender_psid, response);
      resolve('done');

    } catch (err) {
      reject(err);
    }
  })

}

// hàm xử lý Tin nhắn Trả lời Thay đổi Ngày âm lịch
const handleMessageReplyChangeLunarDate = async (sender_psid) => {
  await messengerService.callSendAPI(sender_psid, { "text": "Nhập một ngày dương lịch mà bạn muốn đổi. Ví dụ: 30/6" });
};

//hàm xử lý Tin Nhắn Thay Đổi Ngày Âm Lịch
const handleMessageForChangeLunarDate = async (sender_psid, received_message) => {

  try {
    if (received_message && received_message.text) {
      // Thực hiện logic chuyển đổi ngày
      console.log('Đang thực hiện chuyển đổi ngày...', received_message);

      const dateStr = date.getDayAndMonthAndYear(received_message.text);
      const dateParams = date.validateXX(dateStr);

      let response;
      if (dateParams) {
        const dateChange = Number(dateParams.day);
        const monthChange = Number(dateParams.month);
        const yearChange = Number(dateParams.year) || 2024; //nếu không có mặc định là 2024
        const lunarCalendar = date.convertSolar2Lunar(dateChange, monthChange, yearChange, 7);

        response = {
          "text": `- Ngày: "${dateChange}-${monthChange}-${yearChange}"\n- Âm lịch là ngày: "${lunarCalendar[0]}-${lunarCalendar[1]}-${lunarCalendar[2]}"`
        };
        await messengerService.callSendAPI(sender_psid, response);
        return;
      } else {
        // Trả lời lỗi ngày không hợp lệ
        response = {
          "text": "Sai định dạng ngày. Vui lòng nhập lại theo định dạng dd/mm/yyyy."
        };
        await messengerService.callSendAPI(sender_psid, response);
        return;
      }
    }
    return 'done';
  } catch (err) {
    console.error('Lỗi khi xử lý CHANGE_SCHEDULE:', err);
    throw new Error(err);
  }
};

//hàm xử lý ghi chú cho ngày
const handleNoteForDate = async (sender_psid, received_message) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response;
      const parsedDate = date.getDayAndMonthAndYear(received_message.text);
      const isValidDate = date.validateDay(parsedDate.day);

      if (isValidDate) {
        response = {
          "text": `Hãy nhập nội dung để gợi nhớ đến ngày ${parsedDate.day} hàng tháng.`
        };
      } else {
        response = {
          "text": "Nhập sai ngày vui lòng nhập lại cho đúng ví dụ ngày: 31"
        };
        await messengerService.callSendAPI(sender_psid, response);
        return;
      }

      await messengerService.callSendAPI(sender_psid, response);

      // Lấy thông tin người dùng
      let username = await getUserName(sender_psid);
      let [last_name, first_name] = username.split(' ');

      // Chèn vào cơ sở dữ liệu
      let sql = 'INSERT INTO users (psid, first_name, last_name, date) VALUES (?, ?, ?, ?);';
      db.query(sql, [sender_psid, last_name, first_name, parsedDate.day], (err, result) => {
        if (err) {
          console.error('Lỗi khi lưu/ cập nhật PSID và ngày:', err);
          return reject(err);
        }
        console.log('User PSID và ngày đã được lưu');
        resolve('done');
      });

    } catch (err) {
      console.error('Lỗi khi xử lý handleNoteForDate:', err);
      reject(err);
    }
  });
};

//hàm xử lý tin nhắn ghi chú cho ngày 
const handleMessageNoteForDate = async (sender_psid, received_message, lastMessages) => {
  return new Promise(async (resolve, reject) => {
    let response = {
      "text": "Đặt lịch hàng tháng thành công!!!"
    };

    const lastOwnMessage = lastMessages[1].message;
    console.log("lastOwnMessage: ", lastMessages)

    const messageMatches = lastOwnMessage.match(/Hãy nhập nội dung để gợi nhớ đến ngày (\d+) hàng tháng\./);
    console.log("messageMatches: ", messageMatches)

    const placedDate = messageMatches[1];

    console.log({ placedDate })

    // TODO: Lấy được ngày user đang muốn đặt lịch từ received_message

    // const day = 10; // TODO: remove this line

    console.time('call message api')
    await messengerService.callSendAPI(sender_psid, response);
    console.timeEnd('call message api')
    resolve('done');

    let sql = `INSERT INTO notes (psid, date, note) VALUES (?, ?, ?);`;
    db.query(sql, [sender_psid, placedDate, received_message.text], (err, result) => {
      if (err) {
        return reject(err);
      }
      console.log('Thêm ghi chú thành công!!');
    });
  })
};

//hàm xử lý xem lịch âm hàng tháng
const handleViewLunarCalendarByMonth = async (sender_psid) => {
  try {
    // Lấy tên người dùng
    let username = await getUserName(sender_psid);

    // Lấy danh sách lịch âm từ cơ sở dữ liệu
    let sql = `SELECT notes.date, 
               GROUP_CONCAT(notes.note SEPARATOR ', ') AS notes FROM notes
               LEFT JOIN users ON users.psid = notes.psid and users.date = notes.date 
               WHERE users.psid = ${sender_psid}
               GROUP BY users.date;`;

    db.query(sql, [sender_psid], (err, result) => {
      if (err) {
        console.error('Lỗi khi lấy lịch âm:', err);
        return;
      }

      if (result.length === 0) {
        let response = { text: `Không tìm thấy lịch âm cho người dùng ${username}` };

        return messengerService.callSendAPI(sender_psid, response);
      }

      let responseText = `Xin chào ${username}, đây là danh sách lịch âm đã đặt của bạn:`;
      result.forEach(row => {
        responseText += `\n-------------------------\n- Ngày: ${row.date}\n-------------------------\n- Ghi chú:\n   + ${row.notes.split(', ').join(', \n   + ')}\n`;
      });
      responseText += '-------------------------';

      // Gửi tin nhắn trả lời
      let response = { text: responseText };
      messengerService.callSendAPI(sender_psid, response);
    });
  } catch (err) {
    console.error('Lỗi khi xử lý handleViewLunarCalendar:', err);
  }
};

//Hàm xử lý xem lịch âm hàng năm 
const handleViewLunarCalendarByYear = async (sender_psid) => {
  try {
    // Lấy tên người dùng
    let username = await getUserName(sender_psid);

    // Lấy danh sách lịch âm theo năm từ cơ sở dữ liệu
    let sql = `SELECT * FROM annual_events 
               WHERE psid = ${sender_psid};`;
    console.log("sql", sql)

    db.query(sql, [sender_psid], (err, result) => {
      if (err) {
        console.error('Lỗi khi lấy lịch âm theo năm:', err);
        return;
      }

      if (result.length === 0) {
        let response = { text: `Không tìm thấy lịch âm nào cho năm của người dùng ${username}.` };

        return messengerService.callSendAPI(sender_psid, response);
      }

      let responseText = `Xin chào ${username}, đây là danh sách lịch âm trong năm của bạn:`;
      responseText += '\n--------Thông Tin--------';
      result.forEach(row => {
        responseText += `\n- Sự kiện: ${row.event_name}\n- Ngày:  ${row.event_date}\n- Nội dung: ${row.event_description}\n- ${row.notify === 1 ? "Nhận thông báo hàng năm" : ""}\n-------------------------`;
      });
      responseText += '\n-------------------------';

      // Gửi tin nhắn trả lời
      let response = { text: responseText };
      messengerService.callSendAPI(sender_psid, response);
    });
  } catch (err) {
    console.error('Lỗi khi xử lý handleViewLunarCalendarByYear:', err);
  }
};

// Xử lý postback khi người dùng nhấn CHATGPT_INTEGRATION
const handlePostback = async (sender_psid, received_postback) => {
  const response = {
    text: "Bạn đã kích hoạt ChatGPT. Hãy nhập câu hỏi để tiếp tục!"
  };
  messengerService.callSendAPI(sender_psid, response);
};

// Xử lý tin nhắn cuộc hội thoại với Chatgpt
const handleMessageWithChatgpt = async (sender_psid, received_message) => {
  const userMessage = received_message.text;

  if (!userMessage) return;

  // Lấy ngữ cảnh hội thoại từ cơ sở dữ liệu
  const session = await getSession(sender_psid);

  // Thêm tin nhắn người dùng vào ngữ cảnh
  session.push({ role: "user", content: userMessage });

  // Gửi tin nhắn đến ChatGPT
  const chatGPTResponse = await queryChatGPT(session);

  // Thêm phản hồi của ChatGPT vào ngữ cảnh
  session.push({ role: "assistant", content: chatGPTResponse });

  // Lưu ngữ cảnh mới vào cơ sở dữ liệu
  await saveSession(sender_psid, session);

  // Gửi phản hồi lại Messenger
  const response = { text: chatGPTResponse };
  messengerService.callSendAPI(sender_psid, response);
};

// Hàm gửi tin nhắn đến ChatGPT
const queryChatGPT = async (conversation) => {
  const options = {
    url: "https://api.openai.com/v1/chat/completions",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${OPENAI_API_KEY}`,
    },
    body: JSON.stringify({
      model: "gpt-3.5-turbo", // Sử dụng mô hình ChatGPT 3.5
      messages: conversation, // Gửi tất cả tin nhắn trong cuộc trò chuyện
    }),
  };

  return new Promise((resolve, reject) => {
    request(options, (error, response, body) => {
      if (error) {
        console.error("Error querying ChatGPT:", error);
        resolve("Có lỗi xảy ra khi kết nối với ChatGPT.");
      } else {
        try {
          const parsedBody = JSON.parse(body);

          // Kiểm tra phản hồi có đúng định dạng
          if (parsedBody.choices && parsedBody.choices.length > 0) {
            // Lấy nội dung phản hồi từ ChatGPT
            resolve(parsedBody.choices[0].message.content);
          } else {
            console.error("No valid choices in response");
            resolve("Không có phản hồi hợp lệ từ ChatGPT.");
          }
        } catch (parseError) {
          console.error("Error parsing ChatGPT response:", parseError);
          resolve("Có lỗi xảy ra khi xử lý phản hồi từ ChatGPT.");
        }
      }
    });
  });
};

// Lưu ngữ cảnh hội thoại vào MySQL
const saveSession = async (sender_psid, messages) => {
  const messagesJSON = JSON.stringify(messages);
  try {
    // Sử dụng .promise() để đảm bảo truy vấn trả về Promise
    await db.query(
      `INSERT INTO sessions (sender_psid, messages) VALUES (?, ?)
       ON DUPLICATE KEY UPDATE messages = ?`,
      [sender_psid, messagesJSON, messagesJSON]
    );
  } catch (err) {
    console.error("Error saving session:", err);
  }
};

// Lấy ngữ cảnh hội thoại từ MySQL
const getSession = async (sender_psid) => {
  try {
    // Sử dụng .promise() để đảm bảo truy vấn trả về Promise
    const [rows] = await db.query("SELECT messages FROM sessions WHERE sender_psid = ?", [sender_psid]);
    if (rows.length > 0) {
      return JSON.parse(rows[0].messages);
    } else {
      return []; // Nếu không tìm thấy session thì trả về mảng trống
    }
  } catch (err) {
    console.error("Error getting session:", err);
    return [];
  }
};

//hàm lấy mẫu bắt đầu
let getStartedTemplate = () => {
  let response = {
    "text": "Hãy chọn một trong những yêu cầu sau đây:",
    "quick_replies": [
      {
        "content_type": "text",
        "title": "Xem lịch hôm nay",
        "payload": "SCHEDULE_BOT"
      },
      {
        "content_type": "text",
        "title": "Tạo thông báo lịch",
        "payload": "CREATE_NOTIFICATIONS"
      },
      {
        "content_type": "text",
        "title": "Đổi sang âm lịch",
        "payload": "CHANGE_SCHEDULE"
      }
    ]
  };
  return response;
}

//Hàm lấy trả lời nhanh lịch hàng tháng
let getQuickReplyMonthlyCalendar = () => {
  let response = {
    "text": "Bạn muốn thông báo vào ngày nào hàng tháng? Chọn hoặc nhập số từ 1 đến 31.",
    "quick_replies": [
      {
        "content_type": "text",
        "title": "1",
        "payload": "CREATE_NOTES"
      },
      {
        "content_type": "text",
        "title": "15",
        "payload": "CREATE_NOTES"
      },
      {
        "content_type": "text",
        "title": "30",
        "payload": "CREATE_NOTES"
      }
    ]
  }

  return response;
}

//Hàm lấy mẫu lịch âm
let getLunarTemplate = (psid) => {
  let response = {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type": "button",
        "text": "Đặt lịch để không quên những sự kiện quan trọng. Hãy chọn một lựa chọn sau đây",
        "buttons": [
          {
            "type": "postback",
            "title": "Đặt lịch hàng tháng ",
            "payload": "SetMonthlyCalendar"
          },
          {
            "type": "web_url",
            "url": `${process.env.URL_WEB_VIEW_ORDER}?psid=${psid}`,
            "title": "Đặt Lịch hàng năm ",
            "webview_height_ratio": "tall",
            "messenger_extensions": true
          },
          {
            "type": "postback",
            "title": "Xem lịch đã đặt ",
            "payload": "VIEW_LUNAR_CALENDAR"
          }
        ]
      }
    }
  };
  return response;
}

//Hàm lấy mẫu xem tất cả lịch đã đặt
let getAllLunarTemplate = (psid) => {
  let response = {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type": "button",
        "text": "Chọn xem lịch đã đặt.",
        "buttons": [
          {
            "type": "postback",
            "title": "Xem lịch hàng tháng",
            "payload": "VIEW_LUNAR_CALENDAR_BY_MONTH"
          },
          {
            "type": "postback",
            "title": "Xem lịch hàng năm ",
            "payload": "VIEW_LUNAR_CALENDAR_BY_YEAR"
          }
        ]
      }
    }
  };
  return response;
}

const chatbotService = {
  handleGetstarted,
  handleCREATE_NOTIFICATIONS,
  handleSetMonthlyCalendar,
  handleMessageTodayCalendar,
  handleMessageReplyChangeLunarDate,
  handleMessageForChangeLunarDate,
  handleNoteForDate,
  handleMessageNoteForDate,
  handleViewScheduledEvents,
  handleViewLunarCalendarByMonth,
  handleViewLunarCalendarByYear,
  handlePostback,
  handleMessageWithChatgpt
};

export default chatbotService;
