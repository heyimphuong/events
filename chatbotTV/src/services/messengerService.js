import dotenv from 'dotenv';
import request from 'request';

dotenv.config();
const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;
const callSendAPI = (sender_psid, response) => {
  return new Promise(async (resolve, reject) => {
    try {
      // Construct the message body
      let request_body = {
        "recipient": {
          "id": sender_psid
        },
        "message": response
      };

      request({
        "uri": "https://graph.facebook.com/v20.0/me/messages",
        "qs": { "access_token": PAGE_ACCESS_TOKEN },
        "method": "POST",
        "json": request_body
      }, (err, response, body) => {
        console.log({ body, PAGE_ACCESS_TOKEN })
        if (!err) {
          resolve('Message sent: Line 22');
        } else {
          console.error("Unable to send message: Line 24" + err);
        }
      });
    } catch (err) {
      reject(err);
    }
  })
}

const messengerService = {
  callSendAPI
}

export default messengerService;
