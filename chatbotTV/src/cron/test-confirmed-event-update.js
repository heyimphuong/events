const mysql = require('mysql2');
const request = require('request');

// Cấu hình thông tin của bot
const PAGE_ACCESS_TOKEN = 'EAARf5t2VwwYBO4PHZA4tUHg57MFGU4mvDBVqpZBf4RKU1XGvuvgH2wNaMUH7Ea0krZBJB17bC0qKLKtmGVXEZBt4S0NKtey5AGnmZCq791wH6AFnIuT3O2LbkJPq4viZCvqnZAcujgTXK2gBVR6KXOAS4NL7uOW0KgcpTBPE9XLUDoOiCcePf2ZAZCLddtk8ZCh99vUgZDZD';
const USER_ID = '25919027254408643';
const MYSQL_PASSWORD = '123456';


const db = mysql.createConnection({
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: MYSQL_PASSWORD,
  database: 'chatbot'
});

db.connect((err) => {
  if (err) throw err;
  console.log('Connected to database ');
});

// Hàm gửi tin nhắn qua Messenger API
const sendMessage = (message) => {
  const url = `https://graph.facebook.com/v20.0/me/messages?access_token=${PAGE_ACCESS_TOKEN}`;

  const messageData = {
    messaging_type: 'MESSAGE_TAG',
    tag: 'CONFIRMED_EVENT_UPDATE',
    recipient: { id: USER_ID },
    message: { text: message },
  };

  request.post({
    url: url,
    json: messageData,
  }, (error, response, body) => {
    if (error) {
      console.error('Lỗi khi gửi tin nhắn:', error);
    } else if (response.statusCode === 200) {
      console.log('Tin nhắn đã được gửi thành công:', body);
    } else {
      console.log('Lỗi không xác định:', body);
    }
  });
};

// Quét bảng notes để kiểm tra nếu có ngày trùng với ngày hiện tại
const checkAndSendReminders = () => {
  // const currentDate = new Date().getDate();  // Lấy ngày hiện tại (1-31)
  const currentDate = '31'
  console.log("Date: ", currentDate);

  db.query(`SELECT * FROM notes WHERE date = ?`, [currentDate], (err, results) => {
    if (err) {
      console.error('Lỗi truy vấn:', err);
      return;
    }
    results.forEach((note) => {
      // Gửi tin nhắn nhắc nhở nếu có
      sendMessage(`⚠Nhắc bạn⚠: \nNgày ${currentDate} với Ghi chú: "${note.note}"`);
    });
  });
};

checkAndSendReminders();  // Gọi hàm để kiểm tra và gửi tin nhắn
console.log('Đã quét và kiểm tra thông tin trong bảng notes.');

console.log('Bot đang hoạt động và kiểm tra mỗi ngày...');
