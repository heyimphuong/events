(function (d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) { return; }
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/messenger.Extensions.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'Messenger'));

window.extAsyncInit = function () {
  handleClickButtonSelectDate()
};

function handleClickButtonSelectDate() {
  $("#btnSelectDate").on("click", function () {
    let selectDate = $("#datePicker").val();
    let eventName = $("#eventName").val();
    let eventDescription = $("#eventDescription").val();
    let eventNotifyAnnual = $("#notifyAnnual").prop("checked"); // Sử dụng .prop("checked") để lấy giá trị checked

    console.log("Ngày đã chọn:", selectDate, eventName, eventDescription, eventNotifyAnnual);

    const url = new URLSearchParams(window.location.search);
    let selectPsid = url.get('psid');

    let data = {
      psid: selectPsid,
      eventName: eventName,
      eventDescription: eventDescription,
      datePicker: selectDate,
      eventNotifyAnnual: eventNotifyAnnual // Thêm thuộc tính notifyAnnual vào data
    };

    console.log("Psid:", data.psid);

    // Gửi dữ liệu tới máy chủ
    $.ajax({
      url: `${window.location.origin}/select_date_ajax`,
      method: "POST",
      data: data, 
      success: function (data) {
        console.log('Phản hồi từ máy chủ:', data);
        // Đóng webview
        MessengerExtensions.requestCloseBrowser(function success() {
          // Webview đã đóng
        }, function error(err) {
          console.error('Lỗi khi đóng webview:', err);
        });
      },
      error: function (error) {
        console.error('Lỗi trong yêu cầu AJAX:', error);
      }
    });
  });
}

