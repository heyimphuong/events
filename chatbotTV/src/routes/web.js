import express from "express";
import chatbotcontrollers from "../controllers/chatbotcontrollers.js";

const router = express.Router();

export const initWebRoutes = (app) => {
  router.get("/", chatbotcontrollers.getHomePage);

  router.post('/setup-profile', chatbotcontrollers.setupProfile);
  router.post('/setup-persistent-menu', chatbotcontrollers.setupPersistentMenu);

  router.get("/webhook", chatbotcontrollers.getWebhook);
  router.get("/sendToMessenger", chatbotcontrollers.handleSendToMessenger);
  router.get('/select_date', chatbotcontrollers.handleDateSubmission);
  router.post('/select_date_ajax', chatbotcontrollers.handlePostSelectDate);
  router.post("/webhook", chatbotcontrollers.postWebhook);

  return app.use("/", router);
};

export default initWebRoutes;