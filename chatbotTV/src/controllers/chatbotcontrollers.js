import dotenv from 'dotenv';
import request from 'request';
import makeid from '../helpers/string.js';
import chatbotService from '../services/chatbotService.js';
import messengerService from '../services/messengerService.js';
import date from '../helpers/date.js';
import db from "../database/connection.js";
import fetch from 'node-fetch';

dotenv.config();
const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;
const VERIFY_TOKEN = process.env.VERIFY_TOKEN;
// console.log(VERIFY_TOKEN);

const getHomePage = (req, response) => {
  return response.render('homepage.ejs');
};

let postWebhook = (req, response) => {
  let body = req.body;
  // console.log({ body: JSON.stringify(body) });
  // console.log({ body: JSON.stringify(body.entry) });

  if (body.object === 'page') {
    body.entry.forEach(function (entry) {
      let webhook_event = entry.messaging[0];
      console.log({ webhook_event: JSON.stringify(webhook_event, null, 2) });

      let sender_psid = webhook_event.sender.id;
      console.log('Check Sender PSID: ' + sender_psid);

      if (webhook_event.message && webhook_event.message) {
        handleMessage(sender_psid, webhook_event.message);
        // console.log(">>>Check messenger from postwebhook: ", JSON.stringify(webhook_event.message, null, 2))
        console.log(">>>Check messenger from postwebhook: ", webhook_event.message.text);
        return;
      } else if (webhook_event.postback) {
        handlePostback(sender_psid, webhook_event.postback);
        return;
      }
    });

    response.status(200).send('EVENT_RECEIVED');
  } else {
    response.sendStatus(404);
  }
};

let getWebhook = (req, response) => {
  let mode = req.query['hub.mode'];
  let token = req.query['hub.verify_token'];
  let challenge = req.query['hub.challenge'];

  if (mode && token) {
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {
      // console.log('WEBHOOK_VERIFIED');
      response.set('Content-Type', 'text/plain');
      response.status(200).send(challenge);
    } else {
      response.sendStatus(403);
    }
  }
  response.status(200).send();
};

let handleDateSubmission = (req, res) => {
  return res.render('select_date.ejs');
}

//hàm xử lí đặt lịch hàng năm.
let handlePostSelectDate = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      console.log('Dữ liệu nhận được:', req.body); // Kiểm tra dữ liệu nhận được
      let event_date = date.formatDate(req.body.datePicker);


      let response = {
        "text": `---- THÔNG TIN ----
    \nSự kiện: ${req.body.eventName}
    \nNgày: ${event_date}
    \nNội dung: ${req.body.eventDescription}
    \n${req.body.eventNotifyAnnual === "true" ? "Nhận thông báo hàng năm" : ""}
  `
      };
      await messengerService.callSendAPI(req.body.psid, response);

      // Chuyển đổi eventNotifyAnnual sang 0 hoặc 1
      let eventNotifyAnnual = req.body.eventNotifyAnnual === "true" ? 1 : 0;
      // Chèn vào cơ sở dữ liệu
      let sql = 'INSERT INTO annual_events (psid, event_date, event_name, event_description, notify) VALUES (?, ?, ?, ?, ?);';
      db.query(sql, [req.body.psid, event_date, req.body.eventName, req.body.eventDescription, eventNotifyAnnual], (err, result) => {
        if (err) {
          console.error('Lỗi khi lưu handlePostSelectDate:', err);
          return reject(err);
        }
        console.log('Lưu thành công!!!');
        resolve('done');
      });

    } catch (e) {
      console.log('Lỗi post select date: ', e)
      return res.status(500).json({
        message: "Sever error"
      });
    }
  });
}

async function handleMessage(sender_psid, received_message) {

  let response;

  //check messages for quick replies

  console.log({ received_message1: JSON.stringify(received_message, null, 2) })

  if (received_message.quick_reply && received_message.quick_reply.payload) {
    if (received_message.quick_reply.payload === 'CREATE_NOTIFICATIONS') {
      await chatbotService.handleCREATE_NOTIFICATIONS(sender_psid);
    }
    else if (received_message.quick_reply.payload === 'SCHEDULE_BOT') {
      await chatbotService.handleMessageTodayCalendar(sender_psid);
    }
    else if (received_message.quick_reply.payload === 'CHANGE_SCHEDULE') {
      await chatbotService.handleMessageReplyChangeLunarDate(sender_psid);
      // await chatbotService.handleMessageForChangeLunarDate(sender_psid, received_message);
    }
    else if (received_message.quick_reply.payload === 'CREATE_NOTES') {
      await chatbotService.handleNoteForDate(sender_psid, received_message);
    }
    // else if (received_message.quick_reply.payload === 'CHATGPT_INTEGRATION') {
    //   await chatbotService.handleMessageWithChatgpt(sender_psid, received_message);
    // }
    return;
  }

  // Step 1: Lấy toàn bộ conversations từ sender_psid
  const getConversations = async (sender_psid) => {
    const url = `https://graph.facebook.com/v20.0/${sender_psid}/conversations?access_token=${PAGE_ACCESS_TOKEN}`;

    const response = await fetch(url);
    const data = await response.json();

    console.log("check data from getConversations:", data.data);
    return data.data; // Mảng các cuộc hội thoại
  };

  // Step 2: Lấy 4 messages gần nhất của sender_psid

  const getLastMessages = async (conversation_id) => {
    const url = `https://graph.facebook.com/v20.0/${conversation_id}/messages?fields=message&limit=2&access_token=${PAGE_ACCESS_TOKEN}`;

    const response = await fetch(url);
    const data = await response.json();

    console.log("check data from getLastMessages:", data.data);
    return data.data; // Mảng các tin nhắn
  };
  // Step 3: Kiểm tra xem psid có đang trả lời cho cậu hỏi đổi ngày dương -> âm hay không

  const checkMessageForChangeLunarDate = [
    "Nhập một ngày dương lịch mà bạn muốn đổi. Ví dụ: 30/6"
  ];

  const checkMessageNoteForDate = [
    "Bạn muốn thông báo vào ngày nào hàng tháng? Chọn hoặc nhập số từ 1 đến 31.",
    "Nhập sai ngày vui lòng nhập lại cho đúng ví dụ ngày: 31"
  ];

  const checkMessageRegex = /Hãy nhập nội dung để gợi nhớ đến ngày (\d+) hàng tháng\./;

  const checkMessageSetYearCalendar = [
    "Nhập một ngày âm lịch mà bạn muốn thông báo hàng năm. Ví dụ: 30/6"
  ];

  const checkMessageWithChatgpt = [
    "Bạn đã kích hoạt ChatGPT. Hãy nhập câu hỏi để tiếp tục!",
  ];

  const isRespondingToDateConversion = (messages, checkMessages) => {
    return messages.some(message =>
      message.message &&
      checkMessages.some(checkMessage => message.message.includes(checkMessage))
    );
  };

  const isRespondingToDateConversionbyRegex = (messages, regex) => {
    return messages.some(message =>
      message.message && regex.test(message.message)
    );
  };
  // Step 4: Kiểm tra valid date, và trả lời nếu lỗi ngày
  // Step 5: Trả lời lại câu hỏi
  // TODO: Testing
  // Lấy tất cả các cuộc hội thoại từ sender_psid

  console.time('conversations')
  const conversations = await getConversations(sender_psid);
  console.timeEnd('conversations')

  if (conversations && conversations.length > 0) {
    const conversation_id = conversations[0].id;

    // Lấy 4 tin nhắn gần nhất
    console.time('messages')
    const lastMessages = await getLastMessages(conversation_id);
    console.timeEnd('messages')

    console.log("check lastMessages", lastMessages)

    // Kiểm tra xem có đang trả lời cho câu hỏi đổi ngày hay không
    const isResponding = isRespondingToDateConversion(lastMessages, checkMessageForChangeLunarDate);
    console.log("check isResponding", isResponding);

    // Kiểm tra xem có đang trả lời cho câu hỏi chọn ngày thông báo hay không?
    const isRespondingDate = isRespondingToDateConversion(lastMessages, checkMessageNoteForDate);
    console.log("check isRespondingDate", isRespondingDate);

    // Kiểm tra xem có đang trả lời cho câu hỏi notes hay không?
    const isRespondingNotes = isRespondingToDateConversionbyRegex(lastMessages, checkMessageRegex);
    console.log("check isRespondingNotes", isRespondingNotes);

    // Kiểm tra xem có đang trả lời cho tin nhắn đặt lịch hàng năm hay không?
    const isRespondingMessageSetYearCalendar = isRespondingToDateConversion(lastMessages, checkMessageSetYearCalendar);
    console.log("check isRespondingMessageSetYearCalendar", isRespondingMessageSetYearCalendar);
    
    // Kiểm tra xem có đang trả lời cho tin nhắn chat với ChatGPT hay không?
    const isRespondingMessageWithChatgpt = isRespondingToDateConversion(lastMessages, checkMessageWithChatgpt);
    console.log("check isRespondingMessageWithChatgpt", isRespondingMessageWithChatgpt);

    console.log("check received_message.text: ", received_message.text)
    if (isResponding) {
      await chatbotService.handleMessageForChangeLunarDate(sender_psid, received_message);
    }
    else if (isRespondingDate) {
      await chatbotService.handleNoteForDate(sender_psid, received_message);
    }
    else if (isRespondingNotes) {
      await chatbotService.handleMessageNoteForDate(sender_psid, received_message, lastMessages);
    }
    else if (isRespondingMessageWithChatgpt) {
      await chatbotService.handleMessageWithChatgpt(sender_psid, received_message);
    } 
    else {
      response = {
        "text": `Chúng tôi sẽ sớm phản hồi với bạn!`,
      };
    }
    messengerService.callSendAPI(sender_psid, response);
  }
}

// Checks if the message contains text

const handlePostback = async (sender_psid, received_postback) => {
  let response;

  // get the payload for the postback
  let payload = received_postback.payload;
  // console.log(payload)
  // set the response based on the postback payload
  switch (payload) {
    case "RESTART_BOT":
    case "GET_STARTED":
      await chatbotService.handleGetstarted(sender_psid);
      break;
    case "SCHEDULE_BOT":
      await chatbotService.handleMessageTodayCalendar(sender_psid);
      break;
    case "CREATE_NOTIFICATIONS":
      await chatbotService.handleCREATE_NOTIFICATIONS(sender_psid);
      break;
    case "CHANGE_SCHEDULE":
      await chatbotService.handleMessageReplyChangeLunarDate(sender_psid);
      break;
    case "SetMonthlyCalendar":
      await chatbotService.handleSetMonthlyCalendar(sender_psid);
      break;
    case "VIEW_LUNAR_CALENDAR":
      await chatbotService.handleViewScheduledEvents(sender_psid);
      break;
    case "VIEW_LUNAR_CALENDAR_BY_MONTH":
      await chatbotService.handleViewLunarCalendarByMonth(sender_psid);
      break;
    case "VIEW_LUNAR_CALENDAR_BY_YEAR":
      await chatbotService.handleViewLunarCalendarByYear(sender_psid);
      break;
    case "CHATGPT_INTEGRATION":
      await chatbotService.handlePostback(sender_psid);
      break;
    default:
      response = { "text": `oop! I don't know response with postback ${payload}` }
  }

  console.log(`payload ${payload}`)

  messengerService.callSendAPI(sender_psid, response);
}

let setupProfile = async (req, response) => {
  //call profile facebook API
  let request_body = {
    "get_started": { "payload": "GET_STARTED" },
    "whitelisted_domains": ["https://lunar-chatbot.doran.app/"]
  };

  await request({
    "uri": `https://graph.facebook.com/v20.0/me/messenger_profile?access_token=${PAGE_ACCESS_TOKEN}`,
    "qs": { "access_token": PAGE_ACCESS_TOKEN },
    "method": "POST",
    "json": request_body
  }, (err, response, body) => {
    // console.log({ body, PAGE_ACCESS_TOKEN })
    if (!err) {
      console.log('Setup user profile succeeds!');
    } else {
      console.error("Unable to send message:" + err);
    }
  });

  return response.send("Setup user profile succeeds!");
}


let setupPersistentMenu = async (req, response) => {
  let request_body = {
    "persistent_menu": [
      {
        "locale": "default",
        "composer_input_disabled": false,
        "call_to_actions": [
          {
            "type": "postback",
            "title": "Xem lịch hôm nay",
            "payload": "SCHEDULE_BOT"
          },
          {
            "type": "postback",
            "title": "Tạo thông báo lich",
            "payload": "CREATE_NOTIFICATIONS"
          },
          {
            "type": "postback",
            "title": "Khởi động lại BOT",
            "payload": "RESTART_BOT"
          },
          {
            "type": "postback",
            "title": "Nhập ngày đổi sang âm lịch",
            "payload": "CHANGE_SCHEDULE"
          },
          {
            "type": "postback",
            "title": "Xem lịch đã đặt",
            "payload": "VIEW_LUNAR_CALENDAR"
          },
          {
            "type": "postback",
            "title": "Chat với bot tích hợp CHATGPT",
            "payload": "CHATGPT_INTEGRATION"
          }
        ]
      }
    ]
  };

  await request({
    "uri": `https://graph.facebook.com/v20.0/me/messenger_profile?access_token=${PAGE_ACCESS_TOKEN}`,
    "qs": { "access_token": PAGE_ACCESS_TOKEN },
    "method": "POST",
    "json": request_body
  }, (err, response, body) => {
    // console.log({ body, PAGE_ACCESS_TOKEN })
    if (!err) {
      console.log('Setup persistent menu succeeds!');
    } else {
      console.error("Unable to send message:" + err);
    }
  });

  return response.send("Setup persistent menu succeeds!");
}

let handleSendToMessenger = (req, res) => {
  console.log(makeid())
  return res.render('sendToMessenger.ejs', {
    userRef: makeid()
  });
}

export const chatbotcontrollers = {
  getHomePage,
  getWebhook,
  postWebhook,
  setupProfile,
  setupPersistentMenu,
  handleSendToMessenger,
  handleDateSubmission,
  handlePostSelectDate,
}

export default chatbotcontrollers;