import dotenv from 'dotenv';
import mysql from "mysql2";

dotenv.config();
const MYSQL_PASSWORD = process.env.MYSQL_PASSWORD;
const db = mysql.createConnection({
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: MYSQL_PASSWORD,
  database: 'chatbot'
}).promise();

db.connect((err) => {
  if (err) throw err;
  console.log('Connected to database ');
});

export default db;
