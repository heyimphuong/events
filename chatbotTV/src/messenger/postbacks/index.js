const handlePostback = async (sender_psid, received_postback) => {
  let response;

  // get the payload for the postback
  let payload = received_postback.payload;
  // console.log(payload)
  // set the response based on the postback payload
  switch (payload) {
    case "yes":
      response = { "text": "Kệ bạn =))" };
      // callSendAPI(sender_psid, response);
      break;
    case "no":
      response = { "text": "Bộ bạn buồn lắm hả!?" };
      break;
    case "RESTART_BOT":
    case "GET_STARTED":
      await chatbotService.handleGetstarted(sender_psid);
      break;
    case "SCHEDULE_BOT":
      response = {
        "text": `Hôm nay là: "${calendar}" -- Âm lịch hôm nay là ngày: "${date_lunar}-${month_lunar}-${year_lunar}"`
      };
      break
    case "CREATE_NOTIFICATIONS":
      await chatbotService.handleCREATE_NOTIFICATIONS(sender_psid);
      break;
    case "CHANGE_SCHEDULE":
      response = { "text": "Nhập một ngày dương lịch mà bạn muốn đổi. Ví dụ: 30/6" };
      break;
    default:
      response = { "text": `oop! I don't know respone with postback ${payload}` }
  }

  console.log(`payload ${payload}`)

  messengerService.callSendAPI(sender_psid, response);
}

module.exports = {
  // handlePostback
}
